#include "shared.h"
#include "tkz_store.h"
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

WiFiServer tbSAPServer(5252);
WiFiClient client;

bool startSAP() {
  if (!WiFi.mode(WIFI_AP)) {
    Serial.println("ERROR: could not set WiFi.mode to AP");
    return false;
  }
  if (!WiFi.softAP("Trakeez", NULL)) {
    Serial.println("ERROR: WiFi.softAP()");
    // return false;
  }
  tbSAPServer.begin();
  return true;
}

bool serveAPI() {
  client = tbSAPServer.available();
  if (client) {
    Serial.println("LOG: connected");
    while (client.connected()) {
      while (client.available()) {
        Serial.println("LOG: incomming request");
        StaticJsonDocument<200> doc;
        DeserializationError err = deserializeJson(doc, client);
        if (err.code() != err.Ok) {
          Serial.println(err.c_str());
          break;
        }
        WifiCreds wc;
        String ssid = doc["ssid"];
        String pswd = doc["pswd"];
        if (true) {
          Serial.print("DEBUG: ssid:");
          Serial.println(ssid);
          Serial.print("DEBUG: pswd:");
          Serial.println(pswd);
        }
        wc.ssid = ssid;
        wc.pswd = pswd;
        storeCreds(wc);
      }
    }
    Serial.println("LOG: disconnected");
    return true;
  }
  return false;
}
