#include <ESP8266WiFi.h>
#include "tkz_store.h"

const int TKZ_WIFI_ON = -1;
const int TKZ_WIFI_CANT_START = 0;
const int TKZ_WIFI_CANT_SET_STA = 1;
const int TKZ_WIFI_CANT_SET_HOSTNAME = 2;

int turnOnWiFi() {
  if (WiFi.status() == WL_CONNECTED) {
    char msg[100] = "Wifi is connected with ip: ";
    strcat(msg, WiFi.localIP().toString().c_str());
    return TKZ_WIFI_ON;
  }
  if (!WiFi.mode(WIFI_STA)) {
    return TKZ_WIFI_CANT_SET_STA;
  }
  if (!WiFi.hostname("wemos node")) {
    return TKZ_WIFI_CANT_SET_HOSTNAME;
  }
  WifiCreds wc = getCreds();
  WiFi.begin(wc.ssid.c_str(), wc.pswd.c_str());
  delay(1000);
  if (!WiFi.status() != WL_CONNECTED) {
    return TKZ_WIFI_CANT_START;
  }
  return 1;
}