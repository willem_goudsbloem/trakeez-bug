#ifndef __SHARED_H
#define __SHARED_H

#include <Arduino.h>

struct WifiCreds {
  String ssid;
  String pswd;
};

class Blinker {
public:
  bool run;

  Blinker(uint8_t _led) { led = _led; }

  void blink() {
    if (run && blinkMem < millis()) {
      blinkOn = !blinkOn;
      digitalWrite(led, (blinkOn) ? HIGH : LOW);
      blinkMem = millis() + blinktime;
    }
  }

private:
  uint8_t led;
  u_long blinkMem = 0;
  bool blinkOn;
  const long blinktime = 500;
};

#endif