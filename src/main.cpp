#include "shared.h"
#include "tkz_server.h"
#include "tkz_wifi.h"
#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

typedef void (*cbFunc)(int status, char message[]);

static const u8_t YELLOW_LED = D5;
static const u8_t GREEN_LED = D6;

static const u8_t FUNCTION_SWITCH = D7;
static const u8_t SAP_SWITCH = D1;

const unsigned long TKZ_CHECK_WIFI_CONN_TIME = 10000;
const unsigned long TKZ_TRY_WIFI_CONN_TIME = 5000;
const unsigned long TKZ_POST_RSSI_TIME = 1000;

static const u32_t TKZ_FUNC_LONG_PRESS = 3000;

unsigned long interval, interval2, interval3, interval4;

bool sap = false;

void setup() {
  Serial.begin(9600);
  Serial.println();
  pinMode(YELLOW_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  pinMode(FUNCTION_SWITCH, INPUT_PULLUP);
  pinMode(SAP_SWITCH, INPUT_PULLUP);
  digitalWrite(YELLOW_LED, HIGH);
  pinMode(D2, OUTPUT);
  tone(D2, 300, 1000);
}

WiFiServer ws(5151);
WiFiClient wc;
Blinker grnLED(GREEN_LED);

void loop() {
  grnLED.run = WiFi.isConnected();
  grnLED.blink();
  if (millis() > interval && !sap) {
    if (turnOnWiFi() == TKZ_WIFI_ON) {
      // open tcp socket
      ws.begin();
      interval = millis() + TKZ_CHECK_WIFI_CONN_TIME;
      return;
    } else {
      interval = millis() + TKZ_TRY_WIFI_CONN_TIME;
      return;
    }
  }
  // WiFi is connected)
  if (!wc) {
    wc = ws.available();
  }
  // Broadcast the RSSI
  if (millis() > interval2 && wc.connected() && wc.availableForWrite()) {
    StaticJsonDocument<16> doc;
    doc["data"] = WiFi.RSSI();
    serializeJson(doc, wc);
    interval2 = millis() + TKZ_POST_RSSI_TIME;
  }
  // check for function button longpress
  if (digitalRead(FUNCTION_SWITCH) == LOW) {
    if (interval3 == 0) {
      interval3 = millis() + TKZ_FUNC_LONG_PRESS;
    } else if (interval3 < millis()) {
      ESP.deepSleep(0);
    }
  } else {
    interval3 = 0;
  }
  // check for SAP button longpress
  if (digitalRead(SAP_SWITCH) == LOW) {
    if (interval4 == 0) {
      interval4 = millis() + TKZ_FUNC_LONG_PRESS;
    } else if (interval4 < millis() && !sap) {
      Serial.println("sap server");
      digitalWrite(GREEN_LED, HIGH);
      sap = startSAP();
    }
  } else {
    interval4 = 0;
  }
  // listen to incoming requests
  if (sap) {
    sap = !serveAPI();
    // sap = false;
  }
}
