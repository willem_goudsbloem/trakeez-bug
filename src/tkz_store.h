#ifndef __STORE_H
#define __STORE_H

#include "shared.h"
#include <FS.h>

const char STORE_FILE_NAME[] = "/store.txt";

bool storeCreds(WifiCreds wc) {
  if (!SPIFFS.begin()) {
    Serial.println("ERROR: can't open SPIFF");
    return false;
  }
  SPIFFS.remove(STORE_FILE_NAME);
  File store = SPIFFS.open(STORE_FILE_NAME, "w");
  if (store) {
    Serial.print("ssid stored: '");
    Serial.print(wc.ssid);
    Serial.println("'");
    Serial.print("pswd stored: '");
    Serial.print(wc.pswd);
    Serial.println("'");
    store.println(wc.ssid);
    store.println(wc.pswd);
    store.flush();
    store.close();
  } else {
    Serial.println("ERROR: file not available");
    return false;
  }
  SPIFFS.end();
  return true;
}

WifiCreds getCreds() {
  WifiCreds wc;
  if (!SPIFFS.begin()) {
    Serial.println("ERROR: can't open SPIFF");
    return wc;
  }
  File store = SPIFFS.open(STORE_FILE_NAME, "r");
  if (store.available()) {
    // Serial.println(store.readStringUntil('|'));
    wc.ssid = store.readStringUntil('\r');
    store.read();
    wc.pswd = store.readStringUntil('\r');
      Serial.print("DEBUG: ssid: '");
      Serial.print(wc.ssid);
      Serial.println("'");
      Serial.print("DEBUG: pswd: '");
      Serial.print(wc.pswd);
      Serial.println("'");
    if (wc.ssid == "") {
      Serial.println("ERROR: no ssid found");
      return wc;
    }
    // strcpy(ssid, _ssid.c_str());
    if (wc.pswd == "") {
      Serial.println("ERROR: no password found");
      return wc;
    }
    store.flush();
    store.close();
  }
  SPIFFS.end();
  return wc;
}

#endif